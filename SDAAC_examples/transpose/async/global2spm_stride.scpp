// Copyright (c) 2023 Tecorigin Co., Ltd. All rights reserved.

// *NOTICE TO LICENSE:
// *This source code and/or documentation ("Licensed Deliverables") are subject
// to TECORIGIN intellectual property rights under CHINA and
// international Copyright laws.

// These Licensed Deliverables contained herein is PROPRIETARY and CONFIDENTIAL
// to TECORIGIN and is being provided under the terms and conditions of a
// form of TECORIGIN software license agreement by and between TECORIGIN and
// Licensee ("License Agreement") or electronically accepted by Licensee.

// Notwithstanding any terms or conditions to the contrary in the License
// Agreement, reproduction or disclosure of the Licensed Deliverables to any
// third party without the express written consent of TECORIGIN is prohibited.

// *NOTWITHSTANDING ANY TERMS OR CONDITIONS TO THE CONTRARY IN THE LICENSE
// AGREEMENT, TECORIGIN MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THESE
// LICENSED DELIVERABLES FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT
// EXPRESS OR IMPLIED WARRANTY OF ANY KIND. TECORIGIN DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THESE LICENSED DELIVERABLES, INCLUDING ALL IMPLIED WARRANTIES
// OF MERCHANTABILITY,NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.

// *NOTWITHSTANDING ANY TERMS OR CONDITIONS TO THE CONTRARY IN THE LICENSE
// AGREEMENT, IN NO EVENT SHALL TECORIGIN BE LIABLE FOR ANY SPECIAL, INDIRECT,
// INCIDENTAL, OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING
// FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
// NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
// THE USE OR PERFORMANCE OF THESE LICENSED DELIVERABLES.

// 场景介绍：
// 1. 使用转置接口对Global内存空间的16 * 16的二维矩阵和32 * 32的二维矩阵进行跨步转置操作。
// 2. 在Device端完成跨步转置后将转置接口拷贝到Global内存，并将转置接口与Host端模拟的跨步转置结果进行对比。

#include <time.h>
#include <sdaa_transpose.h>
using namespace sdaa;

#define SIZE_4B 1024
#define DST_SIZE_4B 256
#define HEIGHT_4B 32
#define WIDTH_4B 32

#define GLBMEM_STRIDE_4B 16
#define SECTION_4B 16
#define SECTION_NUM_4B 16


#define SIZE_2B 4096
#define DST_SIZE_2B 1024
#define HEIGHT_2B 64
#define WIDTH_2B 64

#define GLBMEM_STRIDE_2B 32
#define SECTION_2B 32
#define SECTION_NUM_2B 32

#define RAND_SCOP 100
#define RAND_SCOP_SUB 50
#define RAND_SEED 100
#define FLOAT_FACTOR 0.8

// 在Global内存定义64B对齐的数组，用于存储源矩阵
__device__ half g_src_half_1[SIZE_2B] __attribute__((aligned(64)));

__global__ void tanspose_2B(half *g_dst_half, half *g_src_half)
{
    // 仅使用0号计算核心进行功能测试
    if (threadIdx != 0) {
        return;
    }

    // 将源矩阵拷贝到申请的Global内存中
    for (int i = 0; i < SIZE_2B; i++) {
        g_src_half_1[i] = g_src_half[i];
    }

    // 在SPM中申请一段内存空间用于存储转置之后的数据
    half *dst_half = (half *)malloc(DST_SIZE_2B * sizeof(half));

    // 定义转置句柄
    TransposeHandle handle;

    // 设置跨步参数
    Stride stride(SECTION_NUM_2B, GLBMEM_STRIDE_2B * sizeof(half));
    transpose_set_section_size(handle, SECTION_2B  * sizeof(half));

    // 为转置句柄配置跨步参数
    transpose_set_stride(handle, &stride);

    // 检查转置参数是否合法
    unsigned int  check_res = check_transpose_async(dst_half, g_src_half_1,
        HEIGHT_2B, WIDTH_2B, sizeof(half), TransposeGlobalToSpm, handle);
    
    // 如果合法则进行转置
    if (check_res == TRANSPOSE_ASYNC_SUCC) {
        // 使用异步转置接口完成矩阵转置
        transpose_async(dst_half, g_src_half_1, HEIGHT_2B, WIDTH_2B,
                        sizeof(half), TransposeGlobalToSpm, handle);
        // 等待异步转置完成
        transpose_wait(handle);

        // 将转置结果拷贝到Global内存1
        memcpy(g_dst_half, dst_half, DST_SIZE_2B * sizeof(half));
    }

    // 释放申请的SPM内存空间
    free(dst_half);
}

// 在Global内存定义64B对齐的数组，用于存储源矩阵
__device__ int g_src_int_1[SIZE_4B] __attribute__((aligned(64)));

__global__ void tanspose_4B(int *g_dst_int, int *g_src_int)
{
    // 仅使用0号计算核心进行功能测试
    if (threadIdx != 0) {
        return;
    }

    // 将源矩阵拷贝到申请的Global内存中
    for (int i = 0; i < SIZE_4B; i++) {
        g_src_int_1[i] = g_src_int[i];
    }

    // 在SPM中申请一段内存空间用于存储转置之后的数据
    int *dst_int = (int *)malloc(DST_SIZE_4B * sizeof(int));

    // 设置跨步参数
    Stride stride(SECTION_NUM_4B, GLBMEM_STRIDE_4B * sizeof(int));

    // 定义带跨步参数的转置句柄
    TransposeHandle handle(SECTION_4B  * sizeof(int), &stride);

    // 检查转置参数是否合法
    unsigned int check_res = check_transpose_async(dst_int, g_src_int_1,
            HEIGHT_4B, WIDTH_4B, sizeof(int), TransposeGlobalToSpm, handle);

    // 如果合法则进行转置
    if (check_res == TRANSPOSE_ASYNC_SUCC) {
        // 使用异步转置接口完成矩阵转置
        transpose_async(dst_int, g_src_int_1, HEIGHT_4B, WIDTH_4B, sizeof(int),
                        TransposeGlobalToSpm, handle);

        // 等待异步转置完成
        transpose_wait(handle);

        // 将转置结果拷贝到Global内存
        memcpy(g_dst_int, dst_int, DST_SIZE_4B * sizeof(int));
    }

    // 释放申请的SPM内存空间
    free(dst_int);
}

// 模拟矩阵跨步转置操作
template<typename T>
void dma_stride_transpose_4B(T *dst, T *src, unsigned int height,
                             unsigned int width, size_t section,
                             size_t glbmem_stride, unsigned int section_num)
{
    T src_transpose[SIZE_4B];
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            src_transpose[i * height + j] = src[j * width + i];
        }
    }

    int idx = 0;
    for (int i = 0; i < section_num; i++) {
        for (int j = 0; j < section; j++) {
            dst[idx++] = src_transpose[i * (section + glbmem_stride) + j];
        }
    }
}

// 模拟矩阵跨步转置操作
template<typename T>
void dma_stride_transpose_2B(T *dst, T *src, unsigned int height,
                             unsigned int width, size_t section,
                             size_t glbmem_stride, unsigned int section_num)
{
    T src_transpose[SIZE_2B];
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            src_transpose[i * height + j] = src[j * width + i];
        }
    }

    int idx = 0;
    for (int i = 0; i < section_num; i++) {
        for (int j = 0; j < section; j++) {
            dst[idx++] = src_transpose[i * (section + glbmem_stride) + j];
        }
    }
}

void test_4B(int &errnum)
{
    // 申请Host端地址
    int *h_src_int = (int *)malloc(SIZE_4B * sizeof(int));
    int *h_dst_int = (int *)malloc(DST_SIZE_4B * sizeof(int));
    int *h_ref_int = (int *)malloc(DST_SIZE_4B * sizeof(int));

    // 定义随机数种子
    unsigned int local_seed = RAND_SEED;

    // 初始化Host端内存数据
    for (int i = 0; i < SIZE_4B; i++) {
        h_src_int[i] = rand_r(&local_seed) % RAND_SCOP - RAND_SCOP_SUB;
    }

    // 配置使用0号计算核心阵列
    sdaaSetDevice(0);

    // 声明Device端地址
    int *d_src_int = NULL;
    int *d_dst_int = NULL;

    // 使用sdaaMalloc为Device端地址进行空间分配
    sdaaMalloc((void **)(&d_src_int), SIZE_4B * sizeof(int));
    sdaaMalloc((void **)(&d_dst_int), DST_SIZE_4B * sizeof(int));

    // 使用sdaaMemcpy将Host端内存拷贝到Device端
    sdaaMemcpy(d_src_int, h_src_int, SIZE_4B * sizeof(int),
               sdaaMemcpyHostToDevice);
    sdaaMemcpy(d_dst_int, h_dst_int, DST_SIZE_4B * sizeof(int),
               sdaaMemcpyHostToDevice);

    // 启动核函数调用
    tanspose_4B<<<1>>>(d_dst_int, d_src_int);

    // 等待计算核心SPE完成之前的所有请求任务
    sdaaDeviceSynchronize();

    // 使用sdaaMemcpy将Device端数据拷贝到Host端
    sdaaMemcpy(h_dst_int, d_dst_int, DST_SIZE_4B * sizeof(int),
               sdaaMemcpyDeviceToHost);

    // 在Host端模拟矩阵跨步转置操作
    dma_stride_transpose_4B<int>(h_ref_int, h_src_int, WIDTH_4B, HEIGHT_4B,
                                 SECTION_4B, GLBMEM_STRIDE_4B, SECTION_NUM_4B);

    // 将Device端跨步转置结果与模拟结果进行对比
    for (int i = 0; i < DST_SIZE_4B; i++) {
        if (h_dst_int[i] != h_ref_int[i]) {
            printf("h_dst_int[%d]: %d, h_ref_int[%d]: %d\n",
                   i, h_dst_int[i], i, h_ref_int[i]);
            errnum++;
        }
    }

    // 释放Device端内存
    sdaaFree(d_dst_int);
    sdaaFree(d_src_int);

    // 释放Host端内存
    free(h_ref_int);
    free(h_dst_int);
    free(h_src_int);
}


void test_2B(int &errnum)
{
    // 申请Host端地址
    half *h_src_half = (half *)malloc(SIZE_2B * sizeof(half));
    half *h_dst_half = (half *)malloc(DST_SIZE_2B * sizeof(half));
    half *h_ref_half = (half *)malloc(DST_SIZE_2B * sizeof(half));

    // 定义随机数种子
    unsigned int local_seed = RAND_SEED;

    // 初始化Host端内存数据
    for (int i = 0; i < SIZE_2B; i++) {
        h_src_half[i] = rand_r(&local_seed) % RAND_SCOP * FLOAT_FACTOR -
                        RAND_SCOP_SUB;
    }

    // 配置使用0号计算核心阵列
    sdaaSetDevice(0);

    // 声明Device端地址
    half *d_src_half = NULL;
    half *d_dst_half = NULL;

    // 使用sdaaMalloc为Device端地址进行空间分配
    sdaaMalloc((void **)(&d_src_half), SIZE_2B * sizeof(half));
    sdaaMalloc((void **)(&d_dst_half), DST_SIZE_2B * sizeof(half));

    // 使用sdaaMemcpy将Host端内存拷贝到Device端
    sdaaMemcpy(d_src_half, h_src_half, SIZE_2B * sizeof(half),
               sdaaMemcpyHostToDevice);
    sdaaMemcpy(d_dst_half, h_dst_half, DST_SIZE_2B * sizeof(half),
               sdaaMemcpyHostToDevice);

    // 启动核函数调用
    tanspose_2B<<<1>>>(d_dst_half, d_src_half);

    // 等待计算核心SPE完成之前的所有请求任务
    sdaaDeviceSynchronize();

    // 使用sdaaMemcpy将Device端数据拷贝到Host端
    sdaaMemcpy(h_dst_half, d_dst_half, DST_SIZE_2B * sizeof(half),
               sdaaMemcpyDeviceToHost);

    // 在Host端模拟矩阵跨步转置操作
    dma_stride_transpose_2B<half>(h_ref_half, h_src_half, WIDTH_2B,
                                  HEIGHT_2B, SECTION_2B, GLBMEM_STRIDE_2B,
                                  SECTION_NUM_2B);

    // 将Device端跨步转置结果与模拟结果进行对比
    for (int i = 0; i < DST_SIZE_2B; i++) {
        if (h_dst_half[i] != h_ref_half[i]) {
            printf("h_dst_half[%d]: %f, h_ref_half[%d]: %f\n",
                   i, (float)h_dst_half[i], i, (float)h_ref_half[i]);
            errnum++;
        }
    }

    // 释放Device端内存
    sdaaFree(d_dst_half);
    sdaaFree(d_src_half);

    // 释放Host端内存
    free(h_ref_half);
    free(h_dst_half);
    free(h_src_half);
}

int main()
{
    int errnum = 0;

    test_2B(errnum);    // 数据类型为2B的转置测例
    test_4B(errnum);    // 数据类型为4B的转置测例

    if (errnum == 0) {
        printf("check success!\n");
    }

    return 0;
}
